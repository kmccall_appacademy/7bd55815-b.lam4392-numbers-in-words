class Fixnum

  attr_accessor :ONES

  ONES = {
    0 => 'zero',
    1 => 'one',
    2 => 'two',
    3 => 'three',
    4 => 'four',
    5 => 'five',
    6 => 'six',
    7 => 'seven',
    8 => 'eight',
    9 => 'nine'
  }

  TEENS = {
    10 => 'ten',
    11 => 'eleven',
    12 => 'twelve',
    13 => 'thirteen',
    14 => 'fourteen',
    15 => 'fifteen',
    16 => 'sixteen',
    17 => 'seventeen',
    18 => 'eighteen',
    19 => 'nineteen'
  }

  TENS = {
    20 => 'twenty',
    30 => 'thirty',
    40 => 'forty',
    50 => 'fifty',
    60 => 'sixty',
    70 => 'seventy',
    80 => 'eighty',
    90 => 'ninety'
  }

  MAGNITUDE = {
    0 => '',
    2 => 'hundred',
    3 => ' thousand',
    6 => ' million',
    9 => ' billion',
    12 => ' trillion'
  }

  def in_words
    return ONES[0] if zero?
    number = self
    result = ''
    magnitude = 0

    while number > 0
      chunk = number % 1000
      number /= 1000

      chunk_in_word = hundred(chunk)
      if magnitude > 2 && chunk != 0
        chunk_magnitude = MAGNITUDE[magnitude]
        chunk_magnitude += ' ' unless result == ''
      end
      result = "#{chunk_in_word}#{chunk_magnitude}#{result}"
      magnitude += 3
    end
    result
  end

  def hundred(num)
    num_word = []
    hundreds_digit = (num - num % 100) / 100
    other_digits = num % 100

    num_word << ONES[hundreds_digit] << MAGNITUDE[2] unless hundreds_digit.zero?
    num_word << less_than_hundred(other_digits) unless other_digits.zero?

    num_word.join(' ')
  end

  def less_than_hundred(num)
    num_word = []
    ones_digit = num % 10
    tens_digit = num % 100 - ones_digit

    if num < 10
      num_word << ONES[num]
    elsif num < 20
      num_word << TEENS[num]
    elsif num < 100
      num_word << TENS[tens_digit]
      num_word << ONES[ones_digit] unless ones_digit.zero?
    end

    num_word
  end


end
